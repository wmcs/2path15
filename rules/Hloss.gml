#   Deprotonation
#   H loss rule
#   Last review: April, 20, 2018 by Waldeyr Mendes Cordeiro da Silva
#   Christianson, D. W. (2017, September 13). Structural and Chemical Biology of Terpenoid Cyclases. Chemical Reviews. American Chemical Society. https://doi.org/10.1021/acs.chemrev.7b00287
#   Degenhardt, J., Köllner, T. G., & Gershenzon, J. (2009). Monoterpene and sesquiterpene synthases and the origin of terpene skeletal diversity in plants. Phytochemistry, 70(15–16), 1621–1637. https://doi.org/10.1016/j.phytochem.2009.07.030
rule [
    ruleID "Deprotonation (H+)"
    left [
        node [ id 1 label "C+" ]
        node [ id 3 label "H" ]
        edge [ source 1 target 2 label "-" ]
        edge [ source 2 target 3 label "-" ]
    ]
    context [
        node [ id 2 label "C" ]
    ]
    right [
        node [ id 1 label "C" ]
        node [ id 3 label "H+" ]
        edge [ source 1 target 2 label "=" ]
    ]
]