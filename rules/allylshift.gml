# allylic charge shift
# Last review: March, 11, 2018 by Waldeyr Mendes Cordeiro da Silva

rule [
 ruleID "allylic charge shift"
 left [
  node [ id 1 label "C" ]
  node [ id 3 label "C+" ]
  edge [ source 1 target 2 label "=" ]
  edge [ source 2 target 3 label "-" ]
 ]
 context [
  node [ id 2 label "C" ]
 ]
 right [
  node [ id 1 label "C+" ]
  node [ id 3 label "C" ]
  edge [ source 1 target 2 label "-" ]
  edge [ source 2 target 3 label "=" ]
 ]
]
