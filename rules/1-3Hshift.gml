# 1,3 hydride shift rule
# Last review: March, 11, 2018 by Waldeyr Mendes Cordeiro da Silva

rule [
 ruleID "1,3 hydride shift"
 left [
  node [ id  1 label "C+" ]
  node [ id  3 label "C" ]  
 ]
 context [

  node [ id  2 label "C" ]  
  edge [ source  1 target  2 label "-" ]
  edge [ source  2 target  3 label "-" ]
 ]
 right [
  node [ id  1 label "C" ]
  node [ id  3 label "C+" ]  
 ]
]