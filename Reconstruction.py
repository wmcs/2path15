from ConfigClass import *


class Reconstruction(ConfigClass):
	"""
	This class performs the in silico reconstruction of a sesquiterpene metabolic network for a given organism using its transcriptome as input
	"""
	SOURCE_ORGANISM_FASTA_FILE = ""

	def __init__(self):
		ConfigClass.__init__(self)
		print(self.TREE)
		logging.basicConfig(level=logging.ERROR)
		self._ncbiTaxon = self.promptOrganism()
		self.SOURCE_ORGANISM_FASTA_FILE = str("sources/" + self._ncbiTaxon + ".fasta")
		self.BLAST_DIRECTORY = "sources/blast/"
		if not os.path.exists(self.BLAST_DIRECTORY):
			os.makedirs(self.BLAST_DIRECTORY)
		else:
			files = glob.glob(self.BLAST_DIRECTORY + '*')
			for f in files: os.remove(f)

	def promptOrganism(self):
		"""
		This method prompt user for start the reconstruction in a proper way
		:return:
		"""
		g = self.getDatabaseConnection()
		"""This method provides organism data prompting the user"""
		print("\nPlease, search for NCBI taxonomy here: " + self.WARNING +
			  "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi" + self.ENDC)
		taxId = input(
			"\nWhat is the NCBI taxonomy ID of your target organism? ")
		reg = re.compile('^[\d\.]+$')

		while not reg.match(taxId):
			taxId = input(
				self.INPUT_INVALID_MSG + "\nPlease, enter the NCBI taxonomy ID: ")

		while (not self.checkValidNcbiTaxId(taxId)):
			taxId = input(
				self.INPUT_INVALID_MSG + "\nPlease, enter the NCBI taxonomy ID: ")

		# NEW ORGANISM
		Entrez.email = '2path@2path.org'
		taxData = Entrez.read(Entrez.efetch(id=taxId, db="taxonomy", retmode="xml"))
		# ORGANISM NODE
		organismNode = Node("Organism", ncbiTaxon=taxData[0]['TaxId'], ncbiSpecies=taxData[0]['ScientificName'],
							ncbiLineage=taxData[0]['Lineage'].replace(";", ","))

		organismInDatabase = self.checkOrganismInDatabase(taxId)
		if organismInDatabase == 0:  # no organism
			self.storeOrganism(organismNode)
			return taxData[0]['TaxId']
		else:
			answer = input(
				"Organism " + taxId + " already exists. Would you like to " + self.WARNING + "OVERWRITE" + self.ENDC + " it? \n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)
			reg = re.compile('^[q|y|n]{1}$')  # q or y or n
			while not reg.match(answer):
				answer = input(
					self.INPUT_INVALID_MSG + "\n\tPlease, enter: \n\ty for YES\n\tn for NO\n\t" + self.INPUT_ARROW)

			if answer == 'q' or answer == 'n':
				sys.exit("You finished the execution of 2Path for Sesquiterpenes.\n")

			if answer == 'y':
				# DELETE PREVIOUS ORGANISM
				statment = "MATCH (n:Organism{ncbiTaxon:'%s'})-->(s:Sequence) DETACH DELETE n,s" % (taxId)
				g.run(statment)
				self.storeOrganism(organismNode)
				return taxData[0]['TaxId']
		return

	def checkOrganismInDatabase(self, taxId):
		"""
		This method checks if the organism is already in the database
		:param taxId:
		:return:
		"""
		g = self.getDatabaseConnection()
		statment = "MATCH (n:Organism{ncbiTaxon:'%s'}) RETURN COUNT(n) AS qtd" % (taxId)
		result = g.data(statment)
		return result[0]['qtd']

	def checkValidNcbiTaxId(self, taxId: str):
		"""
		This method checks if the informed NCBI taxId is valid
		:param taxId:
		:return:
		"""
		Entrez.email = '2path@2path.org'
		taxData = Entrez.read(Entrez.efetch(
			id=taxId, db="taxonomy", retmode="xml"))
		if len(taxData) == 0:
			return False
		else:
			return True

	def getAllCompoundsWithScenarios(self):
		"""
		This method returns all compound with associated scenarios
		:return: NodeList
		"""
		print("Preparing scenarios...")
		g = self.getDatabaseConnection()
		statment = "MATCH (c:Compound)-–>(n:Scenario) RETURN DISTINCT(c.modName) AS modName"
		results = g.data(statment)
		return results

	def writeFastaFileScenarios(self, compoundsList):
		"""
		This method writes the fasta files with proteins sequences for each compound which is associated to a scenario.
		It returns the path to the written file.
		:param compoundsList: list of Nodes
		:return: String
		"""
		g = self.getDatabaseConnection()
		# get data from  neo4j for fasta files
		sourceForBlastDbFile = open(self.BLAST_DIRECTORY + "2Path15.fasta", "w")
		for compound in compoundsList:
			statment = "MATCH (c:Compound)-–>(n:Scenario) WHERE c.modName = '%s' RETURN DISTINCT(n.ncbiAccession) as ncbiAccession" % \
					   compound['modName']
			ncbiAccessions = g.data(statment)  # list of ncbiAccessions
			for ncbiAccession in ncbiAccessions:
				statment2 = "MATCH (s:Sequence) WHERE s.ncbiAccession = '%s' RETURN s.ncbiDescription AS ncbiDescription, s.ncbiFasta AS ncbiFastancbiFasta" % \
							ncbiAccession['ncbiAccession']
				ncbiSequences = g.data(statment2)
				for ncbiSequence in ncbiSequences:
					fastaContent = ">" + ncbiAccession['ncbiAccession'] + "|" + ncbiSequence['ncbiDescription'] + "\n" + \
								   ncbiSequence['ncbiFastancbiFasta'] + "\n"
					sourceForBlastDbFile.write(str(fastaContent))
		sourceForBlastDbFile.close()
		return sourceForBlastDbFile.name

	def writeFastaFilesForCompoundWithScenarios(self, compoundsList):
		"""
		This method writes the fasta files with proteins sequences for each compound which is associated to a scenario.
		It returns the path to the written files.
		:param compoundsList:
		:return: String
		"""
		g = self.getDatabaseConnection()
		# get data from  neo4j for fasta files
		for compound in compoundsList:
			statment = "MATCH (c:Compound)-–>(n:Scenario) WHERE c.modName = '%s' RETURN DISTINCT(n.ncbiAccession) as ncbiAccession" % \
					   compound['modName']
			ncbiAccessions = g.data(statment)
			for ncbiAccession in ncbiAccessions:
				sourceForBlastDbFile = open(self.BLAST_DIRECTORY + "sequences_for_" + compound['modName'] + ".fasta",
											"w")
				statment2 = "MATCH (s:Sequence) WHERE s.ncbiAccession = '%s' RETURN s.ncbiDescription AS ncbiDescription, s.ncbiFasta AS ncbiFastancbiFasta" % \
							ncbiAccession['ncbiAccession']
				ncbiSequences = g.data(statment2)
				for ncbiSequence in ncbiSequences:
					fastaContent = ">" + ncbiAccession['ncbiAccession'] + "|" + ncbiSequence['ncbiDescription'] + "\n" + \
								   ncbiSequence[
									   'ncbiFastancbiFasta'] + "\n"
					sourceForBlastDbFile.write(str(fastaContent))
					sys.stdout.write('.')
				sourceForBlastDbFile.close()
		print()
		return self.BLAST_DIRECTORY

	def buildBlastDatabase(self, pathToSourceForBlastDbFile):
		"""
		This method build the blast databases for the protein sequences
		:param BLAST_DIRECTORY: String
		:return: None
		"""
		# > /dev/null
		dbType = "prot"
		targetDbFile = str(pathToSourceForBlastDbFile[: -6])
		sourceFastaFile = str(pathToSourceForBlastDbFile)
		cmd = "makeblastdb -in %s -out %s -dbtype %s > /dev/null" % (sourceFastaFile, targetDbFile, dbType)
		os.system(cmd)
		return None

	def blast(self):
		"""
		This method performs the alignments using the Blast program
		"""
		try:
			queryType = "nucl"
			dbType = "prot"
			max_hsps = 5
			evalue = "0.01"
			num_alignments = 10
			blastType = ""
			blastType = "blastx"
			print("Running Sequence annotation...")
			db = self.BLAST_DIRECTORY + "2Path15"
			outFile = "outs/2Path15.xml"  # XML results
			# > /dev/null
			cmd = "%s -query %s -db %s -out %s -evalue %s -outfmt 5 > /dev/null" % (
				blastType, self.SOURCE_ORGANISM_FASTA_FILE, db, outFile, evalue)
			os.system(cmd)
		except Exception as e:
			print(self.FAIL + "\nERROR: " + e + self.ENDC)
			sys.exit(0)
		return outFile

	def parseBlastResults(self, pathToBlastOutFile, evalueThershold):
		"""
		This method is a blast result parser
		:param pathToBlastOutFile:
		:param evalueThershold:
		:return:
		"""
		result_handle = open(pathToBlastOutFile)
		blast_records = NCBIXML.parse(result_handle)
		hits = []
		for blast_record in blast_records:
			for alignment in blast_record.alignments:
				for hsp in alignment.hsps:
					if hsp.expect < evalueThershold:
						hit = [alignment.hit_def.split("|")[0],
							   blast_record.query]  # alignment.hit_def.split("|")[0], hsp.query
						if len(hit) > 0:
							hits.append(hit)
		return hits

	def storeOrganism(self, organismNode):
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.merge(organismNode, "Organism", "ncbiTaxon")
		tx.commit()  # commit transaction

	def storeSequence(self, sequenceNode):
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.merge(sequenceNode, "Sequence", "transcript")
		tx.commit()  # commit transaction

	def storeHas(self, relOrganismSequence):
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.create(relOrganismSequence)
		tx.commit()

	def storeCatalyticActivity(self, relSequenceCompound):
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.create(relSequenceCompound)
		tx.commit()

	def buildInSilicoMetabolicNetwork(self, basedOn, transcript, diff):
		"""
		This metthod performs the in silico reconstruction of the metabolic network
		:param basedOn:
		:param transcript:
		:param diff:
		:return:
		"""
		g = self.getDatabaseConnection()
		selector = NodeSelector(g)
		# get Organism from which the metabolic network is being reconstructed
		organismNode = selector.select("Organism", ncbiTaxon=self._ncbiTaxon).first()
		for record in SeqIO.parse(self.SOURCE_ORGANISM_FASTA_FILE, "fasta"):
			if transcript == str(record.description):
				sequenceNode = Node("Sequence", ncbiDescription="", transcript=transcript,
									basedOn=basedOn, transcriptFasta=str(record.seq))
				self.storeSequence(sequenceNode)
				relOrganismSequence = Relationship(organismNode, "HAS", sequenceNode, diff=diff)
				self.storeHas(relOrganismSequence)
		return None

	def buildCatalyses(self, basedOn, transcript, diff):
		"""
		Theis method creates the relationship CATALYSES where it should exists
		:param basedOn:
		:param transcript:
		:param diff:
		:return:
		"""
		g = self.getDatabaseConnection()
		# SOURCE COMPOUNDS (FPP or NPP)
		selector = NodeSelector(self.getDatabaseConnection())
		transcriptNode = selector.select("Sequence", transcript=transcript).first()
		relsStatment = "MATCH ()-[rel:CATALYZES]->(c:Compound) WHERE rel.ncbiAccession = '%s' RETURN rel, c.modName AS modName" % (
			basedOn)
		relationships = g.data(relsStatment)
		for relationship in relationships:
			compoundNode = selector.select("Compound", modName=relationship['modName']).first()
			relSequenceCompound = Relationship(transcriptNode, "CATALYSES", compoundNode, condition=diff,
											   experiment='in silico', tissue='in silico', ncbiAccession='',
											   pubmedAccession='', scenarioId=relationship['rel']['scenarioId'],
											   ec=relationship['rel']['ec'], iubmb=relationship['rel']['iubmb'],
											   kegg=relationship['rel']['kegg'], rhea=relationship['rel']['rhea'])
			self.storeCatalyticActivity(relSequenceCompound)
		return None


############################################################################
## setup and run
############################################################################
Entrez.email = "2path@2path.org"  # Always tell NCBI who you are
start_time = time.monotonic()
reconstruction = Reconstruction()

print("Reconstructing in silico network for organism: " + reconstruction._ncbiTaxon)
print(reconstruction.WARNING + "\nWARNING: It can take some time." + reconstruction.ENDC)
print()

ncbiCompoundsList = reconstruction.getAllCompoundsWithScenarios()

pathToSourceForBlastDbFile = reconstruction.writeFastaFileScenarios(ncbiCompoundsList)

reconstruction.buildBlastDatabase(pathToSourceForBlastDbFile)

pathToBlastOutFile = reconstruction.blast()
evalueThershold = 0.001
diff = 'single'

for blastHit in reconstruction.parseBlastResults(pathToBlastOutFile, evalueThershold):
	if blastHit:
		sys.stdout.write('.')
		basedOn = blastHit[0]
		transcript = blastHit[1]
		reconstruction.buildInSilicoMetabolicNetwork(basedOn, transcript, diff)
		reconstruction.buildCatalyses(basedOn, transcript, diff)

end_time = time.monotonic()
print("\nAll done. Time: ", timedelta(seconds=end_time - start_time))
