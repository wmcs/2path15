from ConfigClass import *


class Hypergraph(ConfigClass):
	"""
	This class generates the metabolic pathways for sesquiterpenes biosynthesys.
	The metabolic network takes as input the Farnesyl Diphosphate (FPP) molecule and simulates chemical transformations using graph rewrite rules.
	Also, this class stores the hypergraph in a graph database: the Neo4J.
	It works together with the software MedØlDatschgerl, and is the first step for the pipeline for in silico reconstruction of metabolic networks
	Usage::
		$PATH_TO_MØD/mod -f sources/SearchedMolecules.py -f Hypergraph.py
	Dependencies::
		import os
		import glob
		import time
		from datetime import timedelta
		import re
		import uuid
		import logging
		import sys
		from py2neo.packages.httpstream import SocketError
		from py2neo import Graph, Node, Relationship, NodeSelector
		from Bio import Entrez, SeqIO
		from chemspipy import ChemSpider
		from Bio.Blast import NCBIXML

		Neo4j database >= v3.4

	"""

	def __init__(self):
		ConfigClass.__init__(self)
		self._uuid = uuid.uuid4()
		logging.basicConfig(level=logging.FATAL)

	def printTree(self):
		print(self.TREE)

	def getDerivationGraph(self, eductMolecules: list, numberOfIterations: int):
		"""
		This method calculates and returns the derivation graph, which is in fact a hypergraph, based on listed graph grammar rules
		:param eductMolecules: list of smiles in string format
		:param numberOfIterations: int
		:return: dg
		"""
		print("\n####################################################################")
		print("# Generating the hypergraph...")
		print("####################################################################\n")
		######################################
		# RULES LOADING
		######################################
		pushFilePrefix("rules/")
		## FPP initial cyclizations
		ruleGML("1-11CycFromFPP.gml")  # 0
		ruleGML("1-10CycFromFPP.gml")  # 1
		ruleGML("FarnesylC3FromFPP.gml")  # 2
		##NPP  initial cyclizations
		ruleGML("1-11CycFromNPP.gml")  # 3
		ruleGML("1-10CycFromNPP.gml")  # 4
		ruleGML("1-6CycFromNPP.gml")  # 5
		## shifts
		ruleGML("1-3Hshift.gml")  # 6
		ruleGML("1-2Hshift.gml")  # 7
		ruleGML("allylshift.gml")  # 8
		##deprotonation/protonation
		ruleGML("Hloss.gml")  # 9
		ruleGML("H2Ogain.gml")  # 10
		ruleGML("C7protonation.gml")  # 11
		## carbocation rearragments ring closures
		ruleGML("1-6Cyc.gml")  # 12
		ruleGML("2-10Cyc.gml")  # 13
		ruleGML("2-7Cyc.gml")  # 14
		ruleGML("1-11Cyc.gml")  # 15
		## cope rearrangments
		ruleGML("copeRearrByHeat.gml")  # 16
		## oxyreduction
		ruleGML("deltaCadineneOxyReduction.gml")  # 17
		popFilePrefix()

		# general breadth-first expansion strategy
		strat = (
				addSubset(eductMolecules)
				>> repeat[numberOfIterations](inputRules)
		)

		# comput overall charge of molecule
		def overallCharge(a):
			return sum(int(v.charge) for v in a.vertices)

		# calculate the cyclomatic number
		def countCycs(a):
			return a.numEdges - a.numVertices + 1

		# labels
		ls = LabelSettings(LabelType.Term, LabelRelation.Unification)
		# setup and derivation graph calculation
		dg = dgRuleComp(inputGraphs, strat, ls)
		dg.calc()
		return dg

	def getNodeRule(self, rules):
		"""
		This method creates a Neo4J Node for rules
		:param rules: rules
		:return: Node
		"""
		for rule in rules:
			mergeId = uuid.uuid4()
			ruleNode = Node("Rule", mergeId=str(mergeId), modId=rule.id, modName=str(rule.name))
			return ruleNode

	def storeHyperedges(self, dg):
		"""
		This method stores the metabolic network in Neo4J
		:param dg: dg
		:return: None
		"""
		print("\n####################################################################")
		print("# Storing the hypergraph in the Neo4J graph database...")
		print("####################################################################")
		g = self.getDatabaseConnection()  # opening a connection
		g.run("MATCH (n) DETACH DELETE n")
		g.run("CREATE INDEX ON :Compound(modName)")
		g.run("CREATE INDEX ON :Rule(modName)")
		for hyperedge in dg.edges:  # for each hyperedge into derivation graph
			currentHyperegde = dg.findEdge(hyperedge.sources, hyperedge.targets)
			rules = currentHyperegde.rules
			sources = currentHyperegde.sources
			targets = currentHyperegde.targets
			tx = g.begin()  # beginning the transaction
			# rule node
			ruleNode = self.getNodeRule(rules)
			tx.merge(ruleNode, "Rule", "mergeId")
			for source in sources:
				# sources nodes
				if source.graph.isMolecule:
					sourceNode = Node("Compound",
									  modId=str(source.id),
									  modName=str(source.graph.name),
									  modSmiles=str(source.graph.smiles),
									  chemspider="",
									  commonName="",
									  molecularFormula="",
									  molecularWeight="",
									  monoisotopicMass="",
									  averageMass="",
									  nominalMass="",
									  smiles="",
									  imageUrl=""  # ,
									  # scenario=self.getGeneralScenario()
									  )
					# sourceNode.add_label("Sesquiterpenes")
					tx.merge(sourceNode, "Compound", "modId")
					# sources nodes to rule
					sourceToRule = Relationship(sourceNode, "TO", ruleNode)
					tx.create(sourceToRule)
			for target in targets:
				if target.graph.isMolecule:
					targetNode = Node("Compound",
									  modId=str(target.id),
									  modName=str(target.graph.name),
									  modSmiles=str(target.graph.smiles),
									  chemspider="",
									  commonName="",
									  molecularFormula="",
									  molecularWeight="",
									  monoisotopicMass="",
									  averageMass="",
									  nominalMass="",
									  smiles="",
									  imageUrl=""  # ,
									  # scenario=self.getGeneralScenario()
									  )
					# targetNode.add_label("Sesquiterpenes")
					tx.merge(targetNode, "Compound", "modId")
					# sources nodes to rule
					ruleToTarget = Relationship(ruleNode, "TO", targetNode)
					tx.create(ruleToTarget)
			tx.commit()
			sys.stdout.write('.')
		print("\n")
		return None


######################################
## configuration
######################################
hypergraph = Hypergraph()
hypergraph.printTree()
######################################
## setup
######################################
eductMolecules = [fpp, npp, H2O]
############################################################################
## derivation graph calculation
############################################################################
dg = hypergraph.getDerivationGraph(eductMolecules, 5)
############################################################################
## storing derivation graph into graph database
############################################################################
hypergraph.storeHyperedges(dg)
