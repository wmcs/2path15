import re

from chemspipy import ChemSpider

from ConfigClass import *


class Compounds(ConfigClass):
	"""
	This class performs the in silico annotation of the predicted compounds based on the molecules of CHEMSPIDER datbase using the chemical strucure in smiles format as query
	Dependencies::
		import time
		from datetime import timedelta
		import re
		import uuid
		import logging
		import sys
		from py2neo.packages.httpstream import SocketError
		from py2neo import Graph, Node, Relationship, NodeSelector
		from Bio import Entrez, SeqIO
		from chemspipy import ChemSpider
		from Bio.Blast import NCBIXML

		CHEMSPIDER token (see https://developer.rsc.org/user/me/apps)
	"""

	def __init__(self):
		self._uuid = uuid.uuid4()
		ConfigClass.__init__(self)
		logging.basicConfig(level=logging.FATAL)
		print(ConfigClass.TREE)

	def getChemspiderToken(self):
		"""
		This class check the CHEMSPIDER token
		:return: CHEMSPIDER token
		"""
		if not "CHEMSPIDER" in os.environ:
			print(self.FAIL + "\nERROR: CHEMSPIDER TOKEN not found." + self.ENDC)
			print(self.WARNING + "\nSOLUTION: Take your token (https://developer.rsc.org/user/me/apps)" + self.ENDC)
			print(self.WARNING + "Export it: $ export CHEMSPIDER=nnnnnnnn-nnnn-nnnn-nnnn-nnnnnnnnnnnn\n" + self.ENDC)
			sys.exit(0)
		# https://developer.rsc.org/user/me/apps
		# CHEMSPIDER=MyKey
		else:
			return os.environ['CHEMSPIDER']

	def getAllCompoundsFromDatabase(self):
		"""
		This method returns all predicted compounds from Neo4J database
		:return: NodeList
		"""
		g = self.getDatabaseConnection()
		compounds = g.find("Compound")
		return compounds

	def updateCompounds(self, compounds):
		"""
		This method performs the annotation of all predicted compounds, when it is feasible, using thei smiles as query against the CHAMSPIDER database
		:param compounds: NodeList
		:return: None
		"""
		print("\n######################################################")
		print("#  Updating predicted compounds with CHEMSPIDER data")
		print("########################################################\n")
		try:
			cs = ChemSpider(security_token=self.getChemspiderToken())
			for compound in compounds:
				cation = r"(\+)"  # Filter for cations
				if not (re.search(cation, compound["modSmiles"])):  # Filtering  cations
					print("Working on compound: " + compound["modName"])
					resultingCompoundsFromModChemspider = cs.search(compound['modSmiles'])  # result for each compound
					time.sleep(2)
					# There were used the mod smiles, because up/down bonds are not supported in this mod version
					for resultingCompound in resultingCompoundsFromModChemspider:  # each modId = many results
						predicted = r"p_{"  # filter predicted
						if (re.search(predicted, compound["modName"]) and resultingCompound.common_name != ""):
							compound["modName"] = resultingCompound.common_name
						g = self.getDatabaseConnection()
						tx = g.begin()
						compound["chemspider"] = resultingCompound.csid
						compound["commonName"] = resultingCompound.common_name
						compound["molecularFormula"] = resultingCompound.molecular_formula
						compound["molecularWeight"] = resultingCompound.molecular_weight
						compound["monoisotopicMass"] = resultingCompound.monoisotopic_mass
						compound["averageMass"] = resultingCompound.average_mass
						compound["nominalMass"] = resultingCompound.nominal_mass
						compound["smiles"] = resultingCompound.smiles
						compound["imageUrl"] = resultingCompound.image_url
						compound.push()
						tx.commit()
		except:
			print(self.FAIL + "\nERROR: CHEMSPIDER service failed." + self.ENDC)
			print(self.WARNING + "\nPOSSIBLE CAUSE: You have a limited number of accesses." + self.ENDC)
			print(self.WARNING + "\nPOSSIBLE CAUSE: Your internet connection stoped." + self.ENDC)
			sys.exit(0)
		return None


######################################
## setup and run
######################################
start_time = time.monotonic()
compoundObj = Compounds()
compoundObj.getDatabaseConnection()
compounds = compoundObj.getAllCompoundsFromDatabase()
compoundObj.updateCompounds(compounds)  # chemspider dependent
end_time = time.monotonic()
print("\nAll done. Time: ", timedelta(seconds=end_time - start_time))
