from ConfigClass import *


class Scenarios(ConfigClass):
	"""
	This class inserts in the neo4J database the scenarios for sesquiterpenes biosynthsis and links them to the annotated predicted compounds when it is feasible
	Dependencies::
		import os
		import glob
		import time
		from datetime import timedelta
		import re
		import uuid
		import logging
		import sys
		from py2neo.packages.httpstream import SocketError
		from py2neo import Graph, Node, Relationship, NodeSelector
		from Bio import Entrez, SeqIO
		from chemspipy import ChemSpider
		from Bio.Blast import NCBIXML

		It also needs the source file (.csv) with the scenarios retrieved from the literature
	"""

	def __init__(self):
		ConfigClass.__init__(self)
		print(self.TREE)
		logging.basicConfig(level=logging.FATAL)
		with open("sources/scenarios/scenarios.csv", "r") as file:
			lines = file.readlines()
		file.close()
		self._scenarios = lines

	def getAllCompoundsFromDatabase(self):
		"""
		This method returns all predicted compounds from Neo4J database
		:return: NodeList
		"""
		g = self.getDatabaseConnection()
		compounds = g.find("Compound")
		return compounds

	def storeScenarios(self):
		"""
		This method stores the scenarios for sesquiterpenes biosynthsis from a CSV file, and links them to the annotated predicted compounds
		:return: None
		"""
		print("\n####################################################################")
		print("Storing on scenarios...")
		print(self.WARNING + "\nWARNING: It can take some time." + self.ENDC)
		print("####################################################################")
		compounds = self.getAllCompoundsFromDatabase()
		for compound in compounds:
			for scenario in self._scenarios[1:]:
				scenario = scenario.split("\t")
				scenarioId = str(scenario[0].strip("\n"))
				ncbiTaxon = str(scenario[1].strip("\n"))
				ncbiSpecies = str(scenario[2].strip("\n"))
				ncbiAccession = str(scenario[3].strip("\n"))
				pubmedAccession = str(scenario[4].strip("\n"))
				modName = str(scenario[5].strip("\n"))
				experiment = str(scenario[6].strip("\n"))
				tissue = str(scenario[7].strip("\n"))
				condition = str(scenario[8].strip("\n"))
				compoundYield = str(scenario[9].strip("\n"))
				ec = str(scenario[10].strip("\n"))
				kegg = str(scenario[11].strip("\n"))
				rhea = str(scenario[12].strip("\n"))
				iubmb = str(scenario[13].strip("\n"))
				# SCENARIO
				# IS THERE A SCENARIO FOR THE CURRENT COMPOUND?
				if modName == compound['modName'] or modName == compound['commonName']:
					# ORGANISM NODE
					organismNode = Node("Organism", ncbiTaxon=ncbiTaxon, ncbiSpecies=ncbiSpecies)
					self.storeOrganism(organismNode)
					# SEQUENCE NODE
					try:
						handle = Entrez.efetch(db="protein", retmode="text", rettype="fasta", id=ncbiAccession)
						seqRecord = SeqIO.read(handle, "fasta")
						ncbiDescription = seqRecord.description
						ncbiFasta = seqRecord.seq
					except IOError as err:
						print(self.FAIL + "Problem retrieving sequence: " + ncbiAccession + self.ENDC)
						print(self.WARNING + "\nPOSSIBLE CAUSE: ncbi accession do not exists." + self.ENDC)
						sys.exit(0)
					sequenceNode = Node("Sequence", ncbiAccession=ncbiAccession, ncbiDescription=str(ncbiDescription),
										ncbiFasta=str(ncbiFasta))
					self.storeSequence(organismNode, sequenceNode)
					# SOURCE COMPOUNDS (FPP or NPP)
					selector = NodeSelector(self.getDatabaseConnection())
					fppNode = selector.select("Compound", modName="FPP").first()
					nppNode = selector.select("Compound", modName="NPP").first()
					# SCENARIO NODE
					scenarioNode = Node("Scenario",
										scenarioId=scenarioId,
										ncbiTaxon=ncbiTaxon,
										ncbiSpecies=ncbiSpecies,
										ncbiAccession=ncbiAccession,
										pubmedAccession=pubmedAccession,
										modName=modName,
										experiment=experiment,
										tissue=tissue,
										condition=condition,
										compoundYield=compoundYield,
										ec=ec,
										kegg=kegg,
										rhea=rhea,
										iubmb=iubmb
										)
					self.storeScenario(scenarioNode, compound)
					# CATALITYC_ACTIVITY RELATIONSHIP, CASE FPP:
					deep = self.getPathwayDeep(fppNode, compound)
					if deep > 0:
						relSequenceCompound = Relationship(sequenceNode, "CATALYZES", fppNode,
														   scenarioId=scenarioId,
														   ncbiAccession=ncbiAccession,
														   pubmedAccession=pubmedAccession,
														   experiment=experiment,
														   tissue=tissue,
														   condition=condition,
														   ec=ec,
														   kegg=kegg,
														   rhea=rhea,
														   iubmb=iubmb
														   )
						self.storeCatalyticActivity(relSequenceCompound)
					# CATALITYC_ACTIVITY RELATIONSHIP, CASE NPP:
					deep = self.getPathwayDeep(fppNode, compound)
					if deep > 0:
						relSequenceCompound = Relationship(sequenceNode, "CATALYZES", nppNode,
														   scenarioId=scenarioId,
														   ncbiAccession=ncbiAccession,
														   pubmedAccession=pubmedAccession,
														   experiment=experiment,
														   tissue=tissue,
														   condition=condition,
														   ec=ec,
														   kegg=kegg,
														   rhea=rhea,
														   iubmb=iubmb
														   )
						self.storeCatalyticActivity(relSequenceCompound)
		print()
		return None

	def storeOrganism(self, organismNode):
		"""
		This method stores a organism in the neo4j database
		:param organismNode: Node
		:return: None
		"""
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.merge(organismNode, "Organism", "ncbiTaxon")
		tx.commit()  # commit transaction
		return None

	def storeScenario(self, scenarioNode, compoundNode):
		"""
		This method stores a scenario and links it to the correspondent annotated compound
		:param scenarioNode: Node
		:param compoundNode: Node
		:return:
		"""
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.merge(scenarioNode, "Scenario", "scenarioId")
		compoundToScenario = Relationship(compoundNode, "OCCURS", scenarioNode)
		tx.create(compoundToScenario)
		tx.commit()  # commit transaction
		sys.stdout.write('.')

	def storeSequence(self, organismNode, sequenceNode):
		"""
		This method stores the sequence data in the Neo4J database
		:param organismNode: Node
		:param sequenceNode: Node
		:return: None
		"""
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.merge(sequenceNode, "Sequence", "ncbiAccession")
		organismToSequence = Relationship(organismNode, "HAS", sequenceNode)
		tx.create(organismToSequence)
		tx.commit()  # commit transaction
		return None

	def storeCatalyticActivity(self, relSequenceCompound):
		"""
		This method stores the relationship CatalyticActivity (Sequence-->Compound) in the neo4J database
		:param relSequenceCompound:
		:return: None
		"""
		g = self.getDatabaseConnection()
		tx = g.begin()  # begin transaction
		tx.create(relSequenceCompound)
		tx.commit()
		return None

	def getPathwayDeep(self, sourceCompound, targetCompound):
		"""
		This methd return the deep of a pathway between 2 compounds
		:param sourceCompound: Node
		:param targetCompound: Node
		:return: int
		"""
		g = self.getDatabaseConnection()
		deepStatment = 'OPTIONAL MATCH (sources:Compound{modName:"' + sourceCompound[
			"modName"] + '"}),(t:Compound{modName:"' + targetCompound[
						   'modName'] + '"}), pathway = shortestPath((sources)-[*]-(t)) RETURN length(pathway) AS deep'
		deep = g.data(deepStatment)
		return deep[0]["deep"]


############################################################################
## setup and run
############################################################################
Entrez.email = "2path@2path.org"  # Always tell NCBI who you are
start_time = time.monotonic()
scenarios = Scenarios()
scenarios.storeScenarios()
end_time = time.monotonic()
print("\nAll done. Time: ", timedelta(seconds=end_time - start_time))
