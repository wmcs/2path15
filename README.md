# 2Path15

2Path15 is a Python program working together with neo4j database and MedØlDatschgerl.

The 2Path15 aims to reconstruct in silico metabolic networks of terpenes for organisms using their transcript sequences as input.


## Dependencies

It only runs in Linux environments.

### Python dependencies
* [Python >= 3.5](https://www.python.org/downloads/release/python-350/)
* [Py2Neo](https://py2neo.org/v4/) : pip install py2neo
* [BioPython](https://biopython.org/): pip install biopython
* [ChemSpipy](https://chemspipy.readthedocs.io/en/latest/): pip install chemspipy


### Third-party software
* [Neo4J >=3.4](https://neo4j.com/download/)
* [MedØlDatschgerl](https://jakobandersen.github.io/mod/)
* [Blast+](https://www.ncbi.nlm.nih.gov/books/NBK279671/)

### Other dependencies
* [CHEMSPIDER token](https://developer.rsc.org/user/me/apps): export CHEMSPIDER=nnnnnnnn-nnnn-nnnn-nnnn-nnnnnnnnnnnn


## Installation
* $ git clone https://bitbucket.org/wmcs/2path15.git


## Getting start

### Generating a chemical network from a set of rules and storing it in the Neo4J
* $ mod -f sources/SearchedMolecules.py  -f Hypergraph.py

You can change the network removing or adding rules to the Hypergraph.py in the proper lines:
```
#!python
## FPP initial cyclizations
ruleGML("1-11CycFromFPP.gml")  # 0
ruleGML("1-10CycFromFPP.gml")  # 1
ruleGML("FarnesylC3FromFPP.gml")  # 2
##NPP  initial cyclizations
ruleGML("1-11CycFromNPP.gml")  # 3
ruleGML("1-10CycFromNPP.gml")  # 4
ruleGML("1-6CycFromNPP.gml")  # 5
## shifts
ruleGML("1-3Hshift.gml")  # 6
ruleGML("1-2Hshift.gml")  # 7
ruleGML("allylshift.gml")  # 8
##deprotonation/protonation
ruleGML("Hloss.gml")  # 9
ruleGML("H2Ogain.gml")  # 10
ruleGML("C7protonation.gml")  # 11
## carbocation rearragments ring closures
ruleGML("1-6Cyc.gml")  # 12
ruleGML("2-10Cyc.gml")  # 13
ruleGML("2-7Cyc.gml")  # 14
ruleGML("1-11Cyc.gml")  # 15
## cope rearrangments
ruleGML("copeRearrByHeat.gml")  # 16
## oxyreduction
ruleGML("deltaCadineneOxyReduction.gml")  # 17
```
You can change the number of iteration in the line:
```
#!python
dg = hypergraph.getDerivationGraph(eductMolecules, 5)
```

Also, you can change the initial set of molecules in the line:
```
#!python
eductMolecules = [fpp, npp, H2O]
```

### Annotating of predicted compounds
* $ python3 Compounds.py

### Storing scenarios for the annotated compounds
* $ python3 Scenarios.py

### Rescontruting the sesquiterpene metabolic network for a given organism
* $ python3 Recontruction.py

The fasta file with the transcripts of the target organism needs to be available in the folder sources.
The name of the file needs to be its NCBI taxon ID. Example: 327148.fasta is the file for the organism [Copaifera officinalis](https://www.ncbi.nlm.nih.gov/taxonomy/?term=327148)